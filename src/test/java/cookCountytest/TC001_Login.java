package cookCountytest;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_Login extends ProjectMethods{
	
	@BeforeTest
	public void setData()
	{
		testCaseName="TC001_Login";
		testDescription="Login into CookCounty";
		testNodes="Leads";
		author="Arun";
		category="smoke";
        dataSheetName="TC001_LoginLogout";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username,String password) throws InterruptedException
	{
		new cookCountypages.LoginPage()
		.clickAgreebutton()
		.enterEmailAddress(username)
		.enterPassword(password);
		Thread.sleep(10000);
		new cookCountypages.LoginPage().clickLoginbutton()
		.Verifylogin();	
	}
		
}

