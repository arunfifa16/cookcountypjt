package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLead extends ProjectMethods{
	
	public ViewLead() {
	       PageFactory.initElements(driver, this);
		}

		@FindBy(how = How.XPATH,using="(//span[@class='tabletext'])[4]") WebElement eleFirstName;
		public ViewLead verifyFirstElement(String data) {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			verifyExactText(eleFirstName, data);
			return this;
		}
}
