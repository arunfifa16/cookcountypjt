package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLead extends ProjectMethods{
	
	public CreateLead() {
	       PageFactory.initElements(driver, this);
		}

		@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement elefirstName;
		public CreateLead enterFirstName(String data) {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			clearAndType(elefirstName, data);
			return this;
		}
		
		@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement elelastName;
		public CreateLead enterLastName(String data) {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			clearAndType(elelastName, data);
			return this;
		}
		
		@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleCompanyName;
		public CreateLead enterCompanyName(String data) {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			clearAndType(eleCompanyName, data);
			return this;
		}
		
		@FindBy(how=How.CLASS_NAME,using="createLeadForm_lastName") WebElement eleCreatelead;
		public ViewLead clickCreatelead() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleCreatelead);
			return new ViewLead();
		}
		
		
		
		
		
}
	