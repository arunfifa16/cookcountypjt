package cookCountypages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LoginPage extends ProjectMethods{
	public LoginPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH,using="//input[@class='blue_button']") WebElement eleAgreebutton;
	@FindBy(how = How.XPATH,using="//input[@name='txtEmail']") WebElement eleEmailAddress;
	@FindBy(how = How.XPATH,using="//input[@name='txtPassword']") WebElement elePassword;
	@FindBy(how = How.XPATH,using="//input[@value='Login']") WebElement eleLoginbutton;
	
	
	public LoginPage clickAgreebutton() {
		Javascriptclick(eleAgreebutton);
		return this; 
	}
	
	public LoginPage enterEmailAddress(String data) {
		clearAndType(eleEmailAddress, data);
		return this; 
	}
	
	public LoginPage enterPassword(String data) {
		clearAndType(elePassword, data);
		return this; 
	}
	
	public HomePage clickLoginbutton()
	{
		Javascriptclick(eleLoginbutton);
		return new HomePage();
	}

}

