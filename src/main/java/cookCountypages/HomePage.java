package cookCountypages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class HomePage extends ProjectMethods{
	
		public HomePage() {
			//apply PageFactory
			PageFactory.initElements(driver, this); 
		}
		
		@FindBy(how = How.XPATH,using="//div[@class='dropdown pull-right']/a") WebElement eleSignedin;
		
		public  HomePage Verifylogin() {
			verifyDisplayed(eleSignedin);
			return this; 
		}
}
